<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Cache\Adapters\ArrayAdapter;
use Ox\Cache\LayeredCache;
use Ox\Locales\Overload\ArrayOverload;
use Ox\Locales\LocalesManager;
use Ox\Locales\PhpLocalesFilesLoader;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\FlockStore;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'autoload.php';

LayeredCache::init('locales')->setAdapter(LayeredCache::INNER, new ArrayAdapter(), ['namespaced' => false]);

try {
    $cache    = LayeredCache::getCache(LayeredCache::INNER);
    $factory  = new LockFactory(new FlockStore(sys_get_temp_dir()));
    $overload = new ArrayOverload();
    $options  = [
        'localize'        => true,
        'locale_warn'     => true,
        'locale_alert'    => '^',
        'locale_mask'     => '^%s^',
        'localize_ignore' => '/(^CExObject|^CMbObject\.dummy)/',
    ];

    $locale_paths = [
        __DIR__ . '/locales',
    ];

    $translation_paths = [
        __DIR__ . '/modules/*/locales',
    ];

    $loader = new PhpLocalesFilesLoader($locale_paths, $translation_paths);

    $t = microtime(true);

    $manager = LocalesManager::init($cache, $factory, $loader, $overload, $options);
    $manager->loadCoreTranslations('fr');
    $manager->loadCoreTranslations('de');

    dump($manager->translate('fr', 'common-action-Save'));
    dump($manager->translate('fr', 'CActiviteCsARR.back.gestes_complementaires'));
    dump($manager->translate('de', 'CActiviteCsARR.back.gestes_complementaires'));
    dump($manager->translate('fr', 'toto'));
    dump($manager->translate('de', 'common-action-Save'));
    dump($manager->translate('fr', 'common-action-Delete %s and %s ?', 'çà', 'là'));
    dump($manager->translate('fr', 'common-action-Delete %s and %s?', 'çà', 'là'));
} catch (Exception $e) {
    dump($e);
}

dump((microtime(true) - $t));

dump($manager->translate('fr', 'abcd'));
$manager->addTranslation('fr', 'abcd', 'abcd');
dump($manager->translate('fr', 'abcd'));
dump($manager->translate('de', 'abcd'));
