
WORK IN PROGRESS

- [x] Optimisation de l'existant (surtout splitLocale)
- [x] Décorrélation des composants initiaux (CAppUI, CApp, Mutex driver)
- [x] Évaluer pertinence du composant Lock de SF
  - [ ] À terme, composant openxtrem/lock (qui utiliserait par exemple celui de SF) ?
- [ ] Décorréler la Stratégie de mise en cache du Manager => Détermine la performance de l'indexation
  - [ ] Avoir une Stratégie permettant de regrouper les locales par module (contradiction avec l'agrégation post-composer ou refactoring du tableau global)
- [ ] Voir si décorrélation des LocalesInfo est pertinente
- [ ] Réfléchir à comment déterminer quelles sont les langues supportées (actuellement glob du répertoire locales/)
- [ ] Décorréler le File Loader
  - [ ] Voir si le PhpLocalesFilesLoader est nécessaire en l'état (meta.php, locales.php)
  - [ ] Ré-évaluer pertinence du composant SF idoine
- [ ] Clarifier l'API (méthodes publiques) du Manager
  - [x] Factoriser la construction des locales de leur chargement complet
  - [ ] Proposer un chargement de plusieurs langues
  - [ ] Permettre uniquement de charger les locales d'une langue (loadLocales, loadCoreLoales initialisant également les paramètres de l'application)
    - [ ] Suggestion : la recherche d'une traduction ne doit pas tenter de build le cache si celle-ci n'est pas présente
  - [ ] Proposer un chargement des locales d'un module (= chapitre)
  - [ ] Permettre d'ajouter des locales run-time (voir si cela fonctionne toujours)
  - [ ] Supprimer les méthodes non nécessaires à l'usage premier : chargement des locales, recherche de traduction
    - [ ] Refactorer le code tiers dans MB faisant usage des méthodes supprimées pour qu'ils utilisent l'API qui leur est fournie
- [x] Mettre en place un Locale Checker (post-composer ?)
  - [ ] Réfléchir sur la saisie des clefs de locales depuis l'application (utilisation du composant CMbConfig) 
- [ ] Tests unitaires
