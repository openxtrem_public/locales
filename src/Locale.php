<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales;

/**
 * The combination between a language and a country
 */
class Locale
{
    private const DEFAULT_CHARSET = 'UTF-8';

    /** @var string Locale internal name */
    private $internal_name;

    /** @var string Charset (ie. "ISO 8859-1", "UTF-8") */
    private $charset;

    /** @var array List of locale names (ie. "fr_FR.windows-1252", "fr_FR.iso-8859-1", "fr_FR.ISO8859-1", "fr_FR@euro", "fr_FR.utf8", "fr_FR", "fra" */
    private $locale_names = [];

    /** @var int First day of the week (0 = sunday, 1 = monday) */
    private $first_day;

    /** @var string Alpha2 country code denomination (ie. "fr") */
    private $alpha2;

    /** @var string Alpha3 country code denomination (ie. "fra") */
    private $alpha3;

    public function __construct(
        string $name,
        string $charset,
        array $locale_names,
        int $first_day,
        string $alpha2,
        string $alpha3
    ) {
        $this->internal_name = $name;
        $this->charset       = $charset;
        $this->locale_names  = $locale_names;
        $this->first_day     = $first_day;
        $this->alpha2        = $alpha2;
        $this->alpha3        = $alpha3;
    }

    public static function createFromArray(string $name, array $info): self
    {
        return new self(
            $name,
            $info['charset'],
            $info['names'],
            $info['first_day'],
            $info['alpha2'],
            $info['alpha3']
        );
    }

    public function getInternalName(): string
    {
        return $this->internal_name;
    }

    public function getLocaleNames(): array
    {
        return $this->locale_names;
    }

    public function getCharset(): string
    {
        return ($this->charset) ?? self::DEFAULT_CHARSET;
    }
}
