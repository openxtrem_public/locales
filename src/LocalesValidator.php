<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales;

/**
 * Description
 */
class LocalesValidator
{
    /** @var string|null */
    private $last_error;

    /**
     * Validate a locale according to given key => value
     *
     * @param string $key
     * @param string $locale
     *
     * @return bool
     */
    public function validate(string $key, string $locale): bool
    {
        return true;
    }

    /**
     * Get the last generated error
     *
     * @return string|null
     */
    public function getLastError(): ?string
    {
        return $this->last_error;
    }

    /**
     * Set the last error
     *
     * @param string $message
     * @param mixed  ...$args
     *
     * @return void
     */
    private function setError(string $message, ...$args): void
    {
        $this->last_error = sprintf($message, ...$args);
    }
}
