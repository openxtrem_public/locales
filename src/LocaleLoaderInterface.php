<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales;

use Ox\Locales\Exceptions\CouldNotGetLocale;

/**
 * Description
 */
interface LocaleLoaderInterface
{
    public function loadTranslations(Locale $locale): array;

    public function loadAvailableLocales(): void;

    /**
     * @return Locale[]
     */
    public function getAvailableLocales(): array;

    /**
     * @param string $name
     *
     * @return Locale
     * @throws CouldNotGetLocale
     */
    public function getLocale(string $name): Locale;
}
