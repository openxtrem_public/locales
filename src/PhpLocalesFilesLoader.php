<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales;

use Ox\Locales\Exceptions\CouldNotGetLocale;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;
use Symfony\Component\Finder\Finder;

/**
 * Description
 */
class PhpLocalesFilesLoader implements LocaleLoaderInterface
{
    /** @var string */
    private const META_FILENAME = 'meta.php';

    /** @var array */
    private $locale_paths;

    /** @var array */
    private $translation_paths;

    /** @var Locale[] */
    private $available_locales = [];

    /**
     * PhpLocalesFilesLoader constructor.
     *
     * @param array $locale_paths
     * @param array $translation_paths
     */
    public function __construct(array $locale_paths, array $translation_paths)
    {
        $this->locale_paths      = $locale_paths;
        $this->translation_paths = $translation_paths;
    }

    /**
     * @throws CouldNotGetLocale
     */
    public function loadAvailableLocales(): void
    {
        $locales = $this->loadLocales();

        $supported_locales = [];

        foreach ($locales as $_loc) {
            if (!$this->isLocaleReady($_loc)) {
                continue;
            }

            $_locale_info = require $_loc['meta'];

            $_locale = Locale::createFromArray($_loc['name'], $_locale_info);

            $supported_locales[$_locale->getInternalName()] = $_locale;
        }

        $this->available_locales = $supported_locales;
    }

    public function getAvailableLocales(): array
    {
        return $this->available_locales;
    }

    public function loadTranslations(Locale $locale): array
    {
        $this->getLocale($locale->getInternalName());

        $finder = new Finder();

        try {
            $finder->files()->followLinks()->name($locale->getInternalName() . '.php')->in($this->translation_paths);
        } catch (AccessDeniedException $e) {
            throw CouldNotGetLocale::accessDenied($e->getMessage());
        } catch (FileNotFoundException $e) {
            throw CouldNotGetLocale::notFound($e->getMessage());
        }

        if (!$finder->hasResults()) {
            dump('no translation result', $this->translation_paths);

            return [];
        }

        $translations = [];

        foreach ($finder as $_file) {
            $translations = array_merge($translations, require $_file);
        }

        return $translations;
    }

    /**
     * @inheritDoc
     */
    public function getLocale(string $name): Locale
    {
        $locale = ($this->available_locales[$name]) ?? null;

        if ($locale === null) {
            throw CouldNotGetLocale::notRegistered($name);
        }

        return $locale;
    }

    /**
     * @param array $language
     *
     * @return bool
     */
    private function isLocaleReady(array $language): bool
    {
        return is_readable($language['meta']);
    }

    /**
     * @return array
     * @throws CouldNotGetLocale
     */
    private function loadLocales(): array
    {
        $finder = new Finder();

        try {
            $finder->directories()->followLinks()->in($this->locale_paths);
        } catch (AccessDeniedException $e) {
            throw CouldNotGetLocale::accessDenied($e->getMessage());
        } catch (DirectoryNotFoundException $e) {
            throw CouldNotGetLocale::notFound($e->getMessage());
        }

        if (!$finder->hasResults()) {
            return [];
        }

        $locales = [];

        foreach ($finder as $_dir) {
            $locales[] = [
                'name' => $_dir->getRelativePathname(),
                'path' => $_dir->getRealPath(),
                'meta' => $_dir->getRealPath() . DIRECTORY_SEPARATOR . self::META_FILENAME,
            ];
        }

        return $locales;
    }
}
