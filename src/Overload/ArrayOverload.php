<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales\Overload;

use Ox\Locales\Locale;

/**
 * Description
 */
class ArrayOverload implements OverloadInterface
{
    /**
     * @inheritDoc
     */
    public function isSupported(Locale $locale): bool
    {
        return ($locale->getInternalName() === 'fr');
    }

    /**
     * @inheritDoc
     */
    public function getOverriddenTranslations(Locale $locale): array
    {
        return [
            'toto'                 => 'toto',
            'common-action-Save'   => 'Sauver',
            'common-action-Delete' => 'Supprimer',
            'common-action-Delete %s and %s?' => 'Supprimer %s et %s ?',
        ];
    }
}
