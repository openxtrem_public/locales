<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales\Overload;

use Ox\Locales\Locale;

/**
 * Description
 */
interface OverloadInterface
{
    /**
     * Tell whether a given Locale is supported.
     *
     * @param Locale $locale
     *
     * @return bool
     */
    public function isSupported(Locale $locale): bool;

    /**
     * Get the overridden translations for given Locale.
     *
     * @param Locale $locale
     *
     * @return array
     */
    public function getOverriddenTranslations(Locale $locale): array;
}
