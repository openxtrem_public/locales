<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales\Exceptions;

use Exception;

/**
 * Description
 */
class CouldNotGetManager extends Exception
{
    public static function alreadyInitialized(): self
    {
        return new self('Manager is already initialized');
    }

    public static function notInitialized(): self
    {
        return new self('Manager is not initialized');
    }
}
