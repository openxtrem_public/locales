<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales\Exceptions;

use Exception;

/**
 * Description
 */
class CouldNotGetLocale extends Exception
{
    public static function accessDenied(string $message): self
    {
        return new self(
            sprintf('One locale directory has denied access: %s', $message)
        );
    }

    public static function notFound(string $message): self
    {
        return new self(
            sprintf('Locale directory not found: %s', $message)
        );
    }

    public static function notRegistered(string $name): self
    {
        return new self(
            sprintf('Locale not registered: %s', $name)
        );
    }
}
