<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales;

use Ox\Locales\Exceptions\CouldNotGetLocale;
use Ox\Locales\Exceptions\CouldNotGetManager;
use Ox\Locales\Overload\OverloadInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\Lock\LockFactory;

/**
 * Description
 */
class LocalesManager
{
    /** @var string Prefixed by "." to be first in the list */
    private const LOCALES_PREFIX = '.__prefixes__';

    private const COMMON_PREFIX = '__common__';

    private const CACHE_PREFIX = 'locales-v35';
    private const MUTEX_KEY    = self::CACHE_PREFIX . '-' . 'build';

    private const REPLACEMENT_CHARS = [
        '\n' => "\n",
        '\t' => "\t",
    ];

    /** @var self|null */
    private static $instance;

    /** @var CacheInterface */
    private $cache;

    /** @var LockFactory */
    private $lock_factory;

    /** @var OverloadInterface|null */
    private $overload;

    /** @var LocaleLoaderInterface */
    private $loader;

    /** @var array */
    private $options = [];

    /** @var array List of unlocalized strings */
    private $unlocalized = [];

    /** @var array */
    private $runtime_translations = [];

    /**
     * LocalesManager constructor.
     *
     * @param CacheInterface         $cache
     * @param LockFactory            $lock_factory
     * @param LocaleLoaderInterface  $loader
     * @param OverloadInterface|null $overload
     * @param array|null             $options
     */
    private function __construct(
        CacheInterface        $cache,
        LockFactory           $lock_factory,
        LocaleLoaderInterface $loader,
        ?OverloadInterface    $overload = null,
        ?array                $options = []
    ) {
        $this->cache        = $cache;
        $this->lock_factory = $lock_factory;
        $this->loader       = $loader;
        $this->overload     = $overload;

        $this->options = array_merge($this->getDefaultOptions(), $options);

        $this->loader->loadAvailableLocales();

        foreach ($this->loader->getAvailableLocales() as $_locale) {
            $this->unlocalized[$_locale->getInternalName()]          = [];
            $this->runtime_translations[$_locale->getInternalName()] = [];
        }
    }

    /**
     * @param CacheInterface         $cache
     * @param LockFactory            $lock_factory
     * @param PhpLocalesFilesLoader  $loader
     * @param OverloadInterface|null $overload
     * @param array|null             $options
     *
     * @return static
     * @throws CouldNotGetManager
     */
    public static function init(
        CacheInterface        $cache,
        LockFactory           $lock_factory,
        PhpLocalesFilesLoader $loader,
        ?OverloadInterface    $overload = null,
        ?array                $options = []
    ): self {
        if (self::$instance instanceof self) {
            throw CouldNotGetManager::alreadyInitialized();
        }

        return new self($cache, $lock_factory, $loader, $overload, $options);
    }

    /**
     * @return static
     * @throws CouldNotGetManager
     */
    public static function get(): self
    {
        if (!self::$instance instanceof self) {
            throw CouldNotGetManager::notInitialized();
        }

        return self::$instance;
    }

    /**
     * @return array
     */
    private function getDefaultOptions(): array
    {
        return [
            'locale_warn'     => false,
            'localize'        => true,
            'locale_alert'    => '',
            'locale_mask'     => '',
            'localize_ignore' => '',
        ];
    }

    /**
     * Todo: This function should not exists and must be split into CApp|CAppUI
     *
     * @param string $locale_name
     *
     * @throws CouldNotGetLocale
     * @throws InvalidArgumentException
     */
    public function loadCoreTranslations(string $locale_name): void
    {
        $locale = $this->getLocale($locale_name);

        $this->loadTranslations($locale_name);

        $this->setLocaleInformation($locale->getLocaleNames());
        $this->setDefaultCharset($locale->getCharset());
    }

    /**
     * @param string $locale_name
     *
     * @return Locale
     * @throws CouldNotGetLocale
     */
    public function getLocale(string $locale_name): Locale
    {
        return $this->loader->getLocale($locale_name);
    }

    /**
     * Todo: CApp::$encoding = $this->getEncoding()
     *
     * @param string $locale_name
     *
     * @return string|null
     * @throws CouldNotGetLocale
     */
    public function getCharset(string $locale_name): ?string
    {
        return $this->loader->getLocale($locale_name)->getCharset();
    }

    /**
     * @param array $names
     *
     * @return string|false
     */
    public function setLocaleInformation(array $names)
    {
        return setlocale(LC_TIME, $names);
    }

    /**
     * @param string $charset
     *
     * @return bool
     */
    public function setDefaultCharset(string $charset): bool
    {
        return (ini_set('default_charset', $charset) !== false);
    }

    /**
     * @param Locale $locale
     * @param string $key
     *
     * @return string
     */
    private function getCacheKey(Locale $locale, string $key): string
    {
        return self::CACHE_PREFIX . '-' . $locale->getInternalName() . '-' . $key;
    }

    /**
     * @param Locale $locale
     *
     * @return string
     */
    private function getTranslationPrefixesKey(Locale $locale): string
    {
        return $this->getCacheKey($locale, self::LOCALES_PREFIX);
    }

    /**
     * Load locales from cache or build the cache
     *
     * @param string $locale_name
     *
     * @throws CouldNotGetLocale
     * @throws InvalidArgumentException
     */
    public function loadTranslations(string $locale_name): void
    {
        $locale = $this->getLocale($locale_name);

        if ($this->translationsAlreadyInCache($locale)) {
            return;
        }

        $this->buildTranslations($locale);
    }

    /**
     * @param Locale $locale
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    private function translationsAlreadyInCache(Locale $locale): bool
    {
        return ($this->cache->get($this->getTranslationPrefixesKey($locale)) !== null);
    }

    /**
     * @param Locale $locale
     *
     * @throws InvalidArgumentException
     */
    private function buildTranslations(Locale $locale): void
    {
        $lock = $this->lock_factory->createLock(self::MUTEX_KEY, 300, true);

        if (!$lock->acquire(false)) {
            return;
        }

        try {
            if ($this->translationsAlreadyInCache($locale)) {
                $lock->release();

                return;
            }

            // Fix Opcache bug when include locales files (winnt & php7.1)
            if (PHP_OS == 'WINNT' && function_exists('opcache_reset')) {
                opcache_reset();
            }

            $translations = $this->getAllTranslations($locale, true);

            // Prefix = everything before '.' and '-'
            $by_prefix = [];
            $hashes    = [];

            foreach ($translations as $_key => $_value) {
                /** @var string $_prefix */
                /** @var string $_rest */
                /** @var string $_prefix_raw */

                [$_prefix, $_rest, $_prefix_raw] = $this->splitTranslation($_key);

                if (!isset($by_prefix[$_prefix])) {
                    $hashes[$_prefix]    = $_prefix_raw;
                    $by_prefix[$_prefix] = [];
                }

                $by_prefix[$_prefix][$_rest] = $_value;
            }

            foreach ($by_prefix as $_prefix => $_translations) {
                if (!$this->cache->set($this->getCacheKey($locale, $_prefix), $_translations)) {
                    // Error?
                    //throw new Exception('error in cache set: translations');
                }
            }

            if (!$this->cache->set($this->getTranslationPrefixesKey($locale), $hashes)) {
                // Error?
                //throw new Exception('error in cache set: hashes');
            }
        } finally {
            $lock->release();
        }
    }

    /**
     * Todo: addslashes?
     *
     * @param string $locale_name
     * @param string $str
     *
     * @return bool
     * @throws CouldNotGetLocale
     * @throws InvalidArgumentException
     */
    public function isTranslated(string $locale_name, string $str): bool
    {
        $locale     = $this->getLocale($locale_name);
        $translated = $this->translate($locale->getInternalName(), $str);

        if ($this->options['locale_warn']) {
            $char = $this->options['locale_alert'];

            return ($translated !== ($char . $str . $char));
        }

        return ($translated !== $str);
    }

    /**
     * Get locale prefix CRC32 hash
     *
     * @param string $prefix_raw Prefix, raw value
     *
     * @return string
     */
    private function getTranslationPrefixHash(string $prefix_raw): string
    {
        if ($prefix_raw === self::COMMON_PREFIX) {
            return $prefix_raw;
        }

        // Cache of [prefix => crc32(prefix)]
        static $prefixes = [];

        if (isset($prefixes[$prefix_raw])) {
            return $prefixes[$prefix_raw];
        }

        return $prefixes[$prefix_raw] = hash('crc32', $prefix_raw);
    }

    /**
     * Split a locale key into prefix + rest
     *
     * @param string $str The locale key to split
     *
     * @return array
     */
    private function splitTranslation(string $str): array
    {
        $matches = [];
        if (preg_match('/^(?<prefix>[^\-\.]+)(?<rest>[\-\.]+.+)/', $str, $matches)) {
            return [
                $this->getTranslationPrefixHash($matches['prefix']),
                $matches['rest'],
                $matches['prefix'],
            ];
        }

        return [
            $this->getTranslationPrefixHash(self::COMMON_PREFIX),
            $str,
            self::COMMON_PREFIX,
        ];
    }

    /**
     * Localize given statement
     *
     * @param string $locale_name Locale name
     * @param string $str         Statement to translate
     * @param mixed  ...$args     Array or any number of sprintf-like arguments
     *
     * @return string Translated statement
     * @throws CouldNotGetLocale
     * @throws InvalidArgumentException
     */
    public function translate(string $locale_name, string $str, ...$args): string
    {
        $locale = $this->getLocale($locale_name);

        if (empty($str)) {
            return '';
        }

        $str = trim($str);

        if (!$this->options['localize']) {
            return nl2br($str);
        }

        if (isset($this->runtime_translations[$locale->getInternalName()][$str])) {
            return $this->runtime_translations[$locale->getInternalName()][$str];
        }

        [$_prefix, $_rest] = $this->splitTranslation($str);
        $_translations_for_prefix = $this->cache->get($this->getCacheKey($locale, $_prefix));

        if (isset($_translations_for_prefix[$_rest])) {
            $str = $_translations_for_prefix[$_rest];

            if ($args) {
                $str = vsprintf($str, $args);
            }
        } else {
            // Otherwise keep it in a stack...
            if (
                !in_array($str, $this->unlocalized[$locale->getInternalName()])
                && (
                    !$this->options['localize_ignore'] || !preg_match($this->options['localize_ignore'], $str)
                )
            ) {
                $this->unlocalized[$locale->getInternalName()] = $str;
            }

            // ... and decorate
            if ($this->options['locale_warn'] && $this->options['locale_mask']) {
                $str = sprintf($this->options['locale_mask'], $str);
            }
        }

        return nl2br($str);
    }

    /**
     * Get a flattened version of all cached locales
     *
     * @param string $locale_name
     *
     * @return array
     * @throws CouldNotGetLocale
     * @throws InvalidArgumentException
     */
    public function flattenCachedTranslations(string $locale_name): array
    {
        $locale = $this->getLocale($locale_name);

        $prefixes = $this->cache->get($this->getTranslationPrefixesKey($locale));

        if (empty($prefixes)) {
            return [];
        }

        $prefix_cache_keys = [];
        foreach ($prefixes as $_hash => $_prefix) {
            $_cache_key                     = $this->getCacheKey($locale, $_hash);
            $prefix_cache_keys[$_cache_key] = $_hash;
        }

        $prefixed = $this->cache->getMultiple(array_keys($prefix_cache_keys));

        $translations = [];
        foreach ($prefixed as $_hash => $_prefixed) {
            $_prefix = $prefixes[$prefix_cache_keys[$_hash]];

            if (!$_prefixed) {
                continue;
            }

            if ($_prefix === self::COMMON_PREFIX) {
                $_prefix = '';
            }

            foreach ($_prefixed as $_key => $_value) {
                $translations["{$_prefix}{$_key}"] = $_value;
            }
        }

        return $translations;
    }

    /**
     * Get the translations
     *
     * @return array
     */
    public function getAllTranslations(Locale $locale, bool $override = false): array
    {
        $translations = $this->loader->loadTranslations($locale);

        if ($override) {
            $overridden = [];

            if ($this->overload instanceof OverloadInterface && $this->overload->isSupported($locale)) {
                $overridden = $this->overload->getOverriddenTranslations($locale);
            }

            // Merging translations and overridden ones
            $translations = array_merge($translations, $overridden);
        }

        $translations = $this->filterEmpty($translations);

        array_walk($translations, [$this, 'unslash']);

        return $translations;
    }

    /**
     * @param array $strings
     *
     * @return array
     */
    private function filterEmpty(array $strings): array
    {
        return array_filter(
            $strings,
            function ($string) {
                return ($string !== '');
            }
        );
    }

    /**
     * @param string $item
     */
    private function unslash(string &$item): void
    {
        $item = strtr($item, self::REPLACEMENT_CHARS);
    }

    /**
     * Add a locale at runtime.
     *
     * @param string $locale_name
     * @param string $key
     * @param string $value
     *
     * @throws CouldNotGetLocale
     */
    public function addTranslation(string $locale_name, string $key, string $value): void
    {
        if (trim($value) === '') {
            return;
        }

        $locale = $this->getLocale($locale_name);

        $this->runtime_translations[$locale->getInternalName()][$key] = $value;
    }
}
