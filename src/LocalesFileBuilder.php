<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Locales;

/**
 * Description
 */
class LocalesFileBuilder
{
    private const PATTERN_AVAILABLE_LANGUAGES = '/locales/*';
    private const PATTERN_LOCALES_MODULE     = '/modules/*/locales/*.php';
    private const LOCALES_TARGET_DIRECTORY   = '/locales/';

    /** @var string */
    private $root;

    /** @var LocalesValidator */
    private $validator;

    public function __construct(?string $root = null, LocalesValidator $validator = null)
    {
        $this->root      = rtrim(($root) ?: dirname(__DIR__, 3), '/');
        $this->validator = ($validator) ?: new LocalesValidator();
    }

    public function build(): string
    {
        $start = microtime(true);

        $languages = $this->getAvailableLanguages();

        if (empty($languages)) {
            return sprintf('No available language found in %s', $this->getAvailableLanguagesPattern());
        }

        $locales_by_language = array_fill_keys($languages, []);

        foreach ($languages as $_language) {
            @unlink("{$this->root}/locales/{$_language}/all_locales.php");

            $overload = glob("{$this->root}/modules/*/locales/{$_language}.overload.php");
            sort($overload);

            $locales_by_language[$_language] = array_merge(
                glob("{$this->root}/locales/{$_language}/*.php"),
                glob("{$this->root}/modules/*/locales/{$_language}.php"),
                glob("{$this->root}/style/*/locales/{$_language}.php"),
                $overload
            );
        }

        foreach ($locales_by_language as $_language => $_locales_path) {
            $locales = [];

            // Because some files included may output something
            ob_start();
            foreach ($_locales_path as $_locale_path) {
                include_once $_locale_path;
            }
            ob_end_clean();

            foreach ($locales as $_key => $_locale) {
                if (!$this->validator->validate($_key, $_locale)) {
                    return $this->validator->getLastError();
                }
            }

            file_put_contents("{$this->root}/locales/{$_language}/all_locales.php", $this->toPHPFile($locales));
        }

        $duration = round(microtime(true) - $start, 3);

        return sprintf('Generated locales files in %s during %.3f sec', "{$this->root}/locales/", $duration);
    }

    private function getAvailableLanguages(): array
    {
        $languages = [];
        foreach (glob($this->getAvailableLanguagesPattern(), GLOB_ONLYDIR) as $_language) {
            $languages[] = basename($_language);
        }

        return $languages;
    }

    private function getAvailableLanguagesPattern(): string
    {
        return "{$this->root}/locales/*";
    }

    private function toPHPFile(array $data): string
    {
        $content = var_export($data, true) . ';';

        return sprintf(
            <<<EOF
<?php

return %s

EOF
            ,
            $content
        );
    }
}
